//////////////////////////////////////////////////////
//  droneSoundROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneSoundROSModule.h"



using namespace std;




DroneSoundROSModule::DroneSoundROSModule() : DroneModule(droneModule::active,1)
{

    return;
}


DroneSoundROSModule::~DroneSoundROSModule()
{
	close();
	return;
}


bool DroneSoundROSModule::init()
{
    DroneModule::init();



    //end
    return true;
}


void DroneSoundROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);

    //Init
    if(!init())
        cout<<"Error init"<<endl;


    // Get parameters
    ros::param::get("~sound_topic_name", soundTopicName);
    if ( soundTopicName.length() == 0)
    {
        soundTopicName="robotsound";
    }
    std::cout<<"soundTopicName="<<soundTopicName<<std::endl;

    ros::param::get("~file_to_reproduce_topic_name", fileToReproduceTopicName);
    if ( fileToReproduceTopicName.length() == 0)
    {
        fileToReproduceTopicName="file_to_reproduce";
    }
    std::cout<<"file_to_reproduce_topic_name="<<fileToReproduceTopicName<<std::endl;

	

    // Publishers
    soundPub = n.advertise<sound_play::SoundRequest>(soundTopicName, 1);


    // Subscribers
    fileToReproduceSub = n.subscribe(fileToReproduceTopicName, 1, &DroneSoundROSModule::fileToReproduceCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    //moduleStarted=true;


	//End
	return;
}


void DroneSoundROSModule::fileToReproduceCallback(const std_msgs::String::ConstPtr & msg)
{
    if(!moduleStarted)
        return;

    if(msg->data == "")
        soundMsg.arg = "";
    else
        soundMsg.arg = stackPath+"configs/drone"+stringId+"/sounds/"+msg->data;

    soundPublish();

    return;
}


int DroneSoundROSModule::soundPublish()
{
    if(!moduleStarted)
        return 0;

    if(soundMsg.arg=="")
    {
        // Fill the message
        soundMsg.sound = sound_play::SoundRequest::ALL;
        soundMsg.command = sound_play::SoundRequest::PLAY_STOP;;

        // Other args
        soundMsg.arg2 = "";
    }
    else
    {
        // Fill the message
        soundMsg.sound = sound_play::SoundRequest::PLAY_FILE;
        soundMsg.command = sound_play::SoundRequest::PLAY_ONCE;

        // Other args
        soundMsg.arg2 = "";
    }

    // Publish
    soundPub.publish(soundMsg);


    return 1;
}



void DroneSoundROSModule::close()
{
    DroneModule::close();




    return;
}


bool DroneSoundROSModule::resetValues()
{


    return true;
}


bool DroneSoundROSModule::startVal()
{


    //End
    return DroneModule::startVal();
}


bool DroneSoundROSModule::stopVal()
{


    return DroneModule::stopVal();
}


bool DroneSoundROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;



    return true;
}


